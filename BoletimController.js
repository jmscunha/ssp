(function () {
    angular.module("SspApp").controller("BoletimController", [
        "Boletins",
        "Utils",
        "$scope",
        BoletimController
    ]);

    function BoletimController(Boletins, Utils, $scope) {

        var _self = this;
        $scope.maxLength = 2000;

        _self.filtro = {
            limpar: function () {
                delete _self.filtro.numero;
                _self.consultar();
            },
        };

        _self.consultar = function (pagina1) {
            _self.limparMensagens();

            if (pagina1) {
                _self.filtro.paginacao.pagina = 1;
            }
            
            Boletins
                .cadastrados()
                .comNumeroBoletim(_self.filtro.numero)
                .listar(_self.filtro.paginacao)
                .then(function (result) {
                    _self.cadastrados = result.data.boletins;
                    _self.filtro.paginacao = result.data.paginacao;
                })
                .catch(function (result) {
                    _self.error = result.data;
                });
        };

        _self.detalhar = function (boletim) {
            _self.limparMensagens();

            Boletins
                .detalhar(boletim)
                .then(function (result) {
                    _self.boletimDetalhe = result.data.boletim;
                })
                .catch(function (result) {
                    _self.error = result.data;
                });
        };

        _self.editar = function (boletim) {
            _self.limparMensagens();

            Boletins
                .detalhar(boletim)
                .then(function (result) {
                    _self.boletim = result.data.boletim;
                })
                .catch(function (result) {
                    _self.error = result.data;
                });
        };

        _self.limparMensagens = function () {
            delete _self.info;
            delete _self.error;
        };

        _self.modal = {
            remover: function () {
                Boletins
                .remover(_self.boletim)
                .then(function (result) {
                    _self.consultar();
                    _self.info = result.data;

                    var paginaNew = Utils.changePageNew(_self.filtro.paginacao.pagina, _self.filtro.paginacao.registros, _self.filtro.paginacao.limite);
                    _self.filtro.paginacao.pagina = paginaNew;
                })
                .catch(function (result) {
                    _self.error = result.data;
                })
            },
            salvar: function () {
                Boletins
                .salvar(_self.boletim)
                .then(function (result) {
                    _self.consultar();
                    _self.info = result.data;
                })
                .catch(function (result) {
                    _self.error = result.data;
                })
            },
        };

        _self.novo = function () {
            _self.limparMensagens();
            _self.boletim = {};
        };

        _self.selecionar = function (boletim) {
            _self.limparMensagens();
            _self.boletim = angular.copy(boletim);
        };

    }

})();