(function () {
    angular.module("SspApp").service("Boletins", [
    "ApiRequest",
    BoletimService
    ]);

    function BoletimService(ApiRequest) {
        var filtro;
        var url;

        this.cadastrados = function () {
            url = "/cvli/Boletim/Cadastrados";
            filtro = {};
            return this;
        }
        
        this.salvar = function (boletim) {
            url = "/cvli/Boletim/Cadastrar";

            if (!angular.isUndefined(boletim.idBoletim)) {
                url = "/cvli/Boletim/Atualizar";
            }

            var data = {
                idboletim: boletim.idBoletim,
                numero: boletim.numero,
                historico: boletim.historico,
            };

            return ApiRequest.json(url, data);
        }

        this.listar = function (paginacao) {
            filtro.paginacao = paginacao;
            return ApiRequest.json(url, filtro);
        }

        this.comNumeroBoletim = function (numero) {
            filtro.numero = numero;
            return this;
        }

        this.cadastradosSemPaginacao = function () {
            url = "/cvli/Boletim/CadastradosSemPaginacao";
            return ApiRequest.json(url);
        }

        this.detalhar = function (boletim) {
            url = "/cvli/Boletim/Detalhar";
            return ApiRequest.json(url, boletim);
        };

        this.remover = function (boletim) {
            url = "/cvli/Boletim/Remover";
            return ApiRequest.json(url, boletim);
        };
    }
})();